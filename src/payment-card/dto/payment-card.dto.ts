import { Transform } from 'class-transformer';
// import {
//   IsNotEmpty,
//   IsEmail,
//   IsString,
//   IsNumber,
//   Min,
//   Max,
//   Length,
//   Validate,
//   Matches,
// } from 'class-validator';

export class PaymentCardDto {
  email: string;
  card_number: string;
  cvv: string;
  expiration_year: number;
  expiration_month: string;
}
