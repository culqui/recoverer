import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';

export type PaymentCardDocument = HydratedDocument<PaymentCard>;

@Schema()
export class PaymentCard {
  @Prop({ required: true })
  email: string;

  @Prop({ required: true })
  card_number: string;

  @Prop({ required: true })
  cvv: string;

  @Prop({ required: true })
  expiration_year: string;

  @Prop({ required: true })
  expiration_month: string;

  @Prop({ default: new Date() })
  created_at: Date;
}

export const PaymentCardSchema = SchemaFactory.createForClass(PaymentCard);
