import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { PaymentCard, PaymentCardDocument } from './schema';

@Injectable()
export class PaymentCardService {
  constructor(
    @InjectModel(PaymentCard.name)
    private readonly paymentCardModel: Model<PaymentCardDocument>,
  ) {}

  async findAll() {
    const records = await this.paymentCardModel.find();
    return records;
  }

  async findOne(id: string) {
    const record = await this.paymentCardModel
      .findById(id)
      .select({ _id: 0, __v: 0, cvv: 0, created_at: 0 });
    return record;
  }
}
