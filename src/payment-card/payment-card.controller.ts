import { Controller, Get, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { GetUser } from './decorator';
import { PaymentCardService } from './payment-card.service';

@UseGuards(AuthGuard('jwt'))
@Controller('charges')
export class PaymentCardController {
  constructor(private readonly paymentCardService: PaymentCardService) {}

  @Get()
  findOne(@GetUser('id') id: string) {
    return this.paymentCardService.findOne(id);
  }
}
