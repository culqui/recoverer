FROM node:lts-buster  as builder

WORKDIR /app
COPY . .
RUN npm install
RUN npm run build

EXPOSE 4000
CMD ["npm", "run", "start"]